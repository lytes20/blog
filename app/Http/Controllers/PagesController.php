<?php

namespace App\Http\Controllers;

class PagesController extends Controller{

	public function getIndex(){
		return view('pages.welcome');
	}

	public function getAbout(){
		$first = 'gideon';
		$last = 'bamuleseyo';
		$full = $first." ".$last;
		$myemail = 'gideonbamuleseyo@gmail.com';
		$data = [];
		$data['fullname'] = $full;
		$data['mail'] = $myemail;

		return view('pages.about')->withData($data);
	}

	public function getContact(){
		$email_address = 'gideon@kolastudios.com';

		return view('pages.contact')->withMail($email_address);
	}

}