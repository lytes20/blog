@extends('main')

@section('title', '| Contact')

@section('content')
            <div class="row">
                <div class="col-md-12">
                    <h3>Contact Dress Up</h3>
                    <form>
                        <div class="form group">
                            <label name="email">Email</label>
                            <input class="form-control" id="email" name="email">
                        </div>

                        <div class="form group">
                            <label name="email">Subject</label>
                            <input class="form-control" id="subject" name="subject">
                        </div>

                        <div class="form group">
                            <label name="email">Message</label>
                            <textarea class="form-control" id="message" name="message">Type your message here... </textarea> 
                        </div>
                        <hr>

                        <input type="submit"  value="Send Message" class="btn btn-success">

                    </form>
                </div>
            <div>
@endsection

   

