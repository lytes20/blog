@extends('main')

@section('title', '| Homepage')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="jumbotron">
            <h1>Gentleman Apperance</h1>
            <p class="lead">No matter what you may have heard, Appearence matters!</p>
            <p>Welcome to gentleman appearance, a gents fashion blog that gives tips on how to nail that appearance!</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Read More</a></p>
        </div>                
    </div>
</div>  <!-- end of  row --> 


<div class="row">
    <div class="col-md-8">
        <div class="post">
            <h3>Title Post</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <a class="btn btn-primary" href="#">Read More</a>
        </div>
        <hr>

        <div class="post">
            <h3>Title Post</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <a class="btn btn-primary" href="#">Read More</a>
        </div>

        <hr>

        <div class="post">
            <h3>Title Post</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <a class="btn btn-primary" href="#">Read More</a>
        </div>
        <hr>
    </div>

    <div class="col-md-3 col-md-offset-1">
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged
        
    </div>
</div>   
@endsection 


   
   